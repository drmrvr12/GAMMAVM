﻿/*
    ISC-No-Nuclear License

    Copyright (c) 2022 Wiktor Cielebon

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.

    You acknowledge that this software is not designed, licensed or intended for 
    use in the design, construction, operation or maintenance of any nuclear facility.

 */

using GVM;
using System;
using System.Text;

namespace GDOC {
    class Program {
        static void Main(string[] args) {
            if (args.Length == 0) {
                Console.WriteLine("Usage:" +
                    "\r\n\r\n  GDOC [INPUT]" +
                    "\r\n" +
                    "\r\nINPUT - Input DLL file with the architecture definition.");
            } else {
                IArchitecture arc = GetArchitectureFromFile(args[0]);
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("OPCODE\tMNEMONIC\t\tLENGTH");
                foreach (var X in arc.Instructions) {
                    sb.Append("0x" + X.Opcode.ToString("X2") + "\t" + X.Mnemonic + " ");
                    if (X.OperandTypes != null) {
                        for (int I = 0; I < X.OperandTypes.Length; I++) {
                            switch (X.OperandTypes[I]) {
                                case GVM.Assembler.OperandType.Register:
                                    sb.Append("[REG]");
                                    break;
                                case GVM.Assembler.OperandType.QWORD:
                                    sb.Append("[QWORD]");
                                    break;
                                case GVM.Assembler.OperandType.DWORD:
                                    sb.Append("[DWORD]");
                                    break;
                                case GVM.Assembler.OperandType.WORD:
                                    sb.Append("[WORD]");
                                    break;
                                case GVM.Assembler.OperandType.BYTE:
                                    sb.Append("[BYTE]");
                                    break;
                                case GVM.Assembler.OperandType.SINGLE:
                                    sb.Append("[SINGLE]");
                                    break;
                                default:
                                    break;
                            }
                            if (I < X.OperandTypes.Length - 1) sb.Append(", ");
                        }
                    }
                    sb.Append("\t");
                    sb.Append(X.Length + 1);
                    sb.AppendLine();
                }
                System.IO.File.WriteAllText(System.IO.Path.ChangeExtension(args[0], "txt"), sb.ToString());
            }
        }
        public static IArchitecture GetArchitectureFromFile(string Filename) {
            if (System.IO.File.Exists(Filename)) {
                Type T = typeof(IArchitecture);
                System.Reflection.Assembly a = System.Reflection.Assembly.LoadFrom(Filename);
                foreach (Type TP in a.GetTypes()) {
                    if (TP.IsNotPublic)
                        continue;
                    if (T.IsAssignableFrom(TP)) {
                        IArchitecture arch = (IArchitecture)Activator.CreateInstance(TP);
                        return arch;
                    }
                }
            }
            return null;
        }
    }
}
