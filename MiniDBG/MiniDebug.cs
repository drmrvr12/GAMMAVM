﻿/*
    ISC-No-Nuclear License

    Copyright (c) 2022 Wiktor Cielebon

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.

    You acknowledge that this software is not designed, licensed or intended for 
    use in the design, construction, operation or maintenance of any nuclear facility.

 */

using GRT;
using GVM;
using GVM.Assembler;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiniDBG {
    public partial class MiniDebug : Form, IModule {
        public MiniDebug() {
            InitializeComponent();
        }

        public Machine Caller { get; set; }
        public Boolean HaltsExit => true;
        bool IsClosing = false;
        ConcurrentQueue<string> disassembly = new ConcurrentQueue<string>();

        public void End() {
            IsClosing = true;
            this.Close();
        }

        
        void IModule.Load() {
            Task.Factory.StartNew(() => {
                Caller.RaiseDisassembly = true;
                Caller.Disassembly += this.Caller_Disassembly;
                Thread dasm = new Thread(() => {
                    while (true) {
                        List<string> da = new List<string>();
                        Stopwatch sw = new Stopwatch();
                        sw.Start();
                        while (disassembly.Count != 0 && sw.ElapsedMilliseconds < 250) {
                            disassembly.TryDequeue(out string d);
                            if (d != null)
                                da.Add(d);
                        }
                        if (da.Count!=0)
                            this.Invoke((Action)delegate {
                                //
                                // TODO: ListView /w timestamps, JMP, and exec time
                                //
                                DisassemblyListBox.Items.AddRange(da.ToArray());
                                DisassemblyListBox.SelectedIndex = DisassemblyListBox.Items.Count - 1;
                                List<string> mem = new List<string>();
                                StringBuilder m = new StringBuilder();
                                StringBuilder ascii = new StringBuilder();
                                for (ulong i = 0; i <= Caller.Memory.BlocksAllocated * Caller.Memory.BlockSize; i++) {
                                    byte memr = Caller.Memory[i];
                                    m.Append(memr.ToString("X2") + " ");
                                    char c = System.Text.Encoding.ASCII.GetString(new[] { memr })[0];
                                    ascii.Append(c < ' ' | c > '\x7f' ? ' ' : c);
                                    if (i % 32 == 0) {
                                        mem.Add(m.ToString() + "  " + ascii.ToString());
                                        m.Clear();
                                        ascii.Clear();
                                    }
                                }
                                MemoryListBox.Items.Clear();
                                MemoryListBox.Items.AddRange(mem.ToArray());
                            });
                        if (IsClosing) break;
                    }
                });
                // Pro tip: Setting this to true will not prevent the program from closing.
                dasm.IsBackground = true; 
                dasm.Start();
                System.Windows.Forms.Application.Run(this);
            });
        }

        private void Caller_Disassembly(Object sender, DisassemblyEventArgs e) {
            Instruction ins = e.Instruction;
            StringBuilder sb = new StringBuilder();
            if (ins.OperandTypes != null) {
                sb.Append(" ; ");
                bool hasRegs = false;
                for (int I = 0; I < ins.OperandTypes.Length; I++) {
                    OperandType type = ins.OperandTypes[I];
                    if (type == OperandType.Register) {
                        hasRegs = true;
                        var num = Caller.GetRegister(e.Args[I]);
                        string ns = "";
                        if (num is ulong) ns = "0x" + ((ulong)num).ToString("X16");
                        if (num is uint) ns = "0x" + ((uint)num).ToString("X8");
                        if (num is float) ns = ((float)num).ToString();
                        sb.Append(Machine.GetRegisterMnemonic(e.Args[I]) + " = " + ns + ", ");
                    }
                }
                if (!hasRegs) sb.Clear();
            }
            disassembly.Enqueue("0x" + e.PC.ToString("X16") + ": " + e.Disassembly + sb.ToString().TrimEnd(',', ' '));
        }
    }
}
