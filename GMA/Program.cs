﻿/*
    ISC-No-Nuclear License

    Copyright (c) 2022 Wiktor Cielebon

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.

    You acknowledge that this software is not designed, licensed or intended for 
    use in the design, construction, operation or maintenance of any nuclear facility.

 */

using GVM.Assembler;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace GMA {
    class Program {
        static void Main(string[] args) {
            if (args.Length == 0) {
                ShowHelp();
            } else {
                string input = null;
                string output = null;
                bool hasargs = false;
                if (args.Length == 1) {
                    if (args[0].StartsWith("/")) {
                        hasargs = true;
                    } else {
                        input = args[0];
                    }
                } else hasargs = true;

                if (hasargs) {
                    foreach (var X in args) {
                        switch (X.ToUpper()) {
                            case "/?":
                                ShowHelp();
                                return;
                            case string w when w.StartsWith("/I:"):
                                input = GetStringFromArgument(X);
                                break;
                            case string w when w.StartsWith("/O:"): 
                                output = GetStringFromArgument(X);
                                break;
                            default:
                                Console.WriteLine("Invalid argument: \"{0}\".", X);
                                return;
                        }
                    }
                }

                Console.WriteLine("GAMMAVM Assembler, v" + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version);
                if (input == null) {
                    Console.WriteLine("No input specified.");
                    return;
                }
                Console.WriteLine("\r\nInput: " + input);
                if (output == null)
                    output = System.IO.Path.GetFileNameWithoutExtension(input) + ".BIN";
                Console.WriteLine("Output: " + output);
                var mach = new GAMMA64.StdGAMMA64();
                var instructions = mach.Instructions;
                Console.WriteLine("CPU: " + mach.Name + ", " + mach.Architecture + "bits.");
                Console.WriteLine("Loaded " + instructions.Length + " instructions.");
                string[] lines = null;
                if (System.IO.File.Exists(input))
                    lines = System.IO.File.ReadAllLines(input);
                else {
                    Error(Environment.CommandLine, input, -1, "Could not load \"" + input + "\" - file not found.");
                    Environment.Exit(2);
                }
                GVM.Assembler.BinaryBuilder bb = new BinaryBuilder(lines, mach);
                bb.Error += (x, e) => {
                    Error(e.LineText, e.Argument, e.LineNumber, e.Message);
                    Environment.Exit(1);
                };
                Stopwatch sw = new Stopwatch();
                sw.Start();
                var res = bb.Build();
                sw.Stop();
                System.IO.File.WriteAllBytes(output, res.ToArray());
                Console.WriteLine("\r\nCompleted in " + sw.ElapsedMilliseconds + "ms, " + res.Length + " bytes.");
            }

        }

        private static void ShowHelp() => Console.WriteLine("Usage:" +
            "\r\n" +
            "\r\n   GMA [FILE]" +
            "\r\n" +
            "\r\n   GMA [/?] [/I:FILE] [/O:FILENAME]" +
            "\r\n" +
            "\r\nFILE - Input file (typically *.ASM). Must be the only argument." +
            "\r\n\r\nor\r\n" +
            "\r\n/? - Shows this text" +
            "\r\n/I:FILE - Input file (typically *.ASM)" +
            "\r\n/O:FILENAME - Output file (typically *.BIN)");

        internal static void Error(string lineText, string arg, int line, string message, string messageLate = null, string reserved = null) {
            Console.ForegroundColor = ConsoleColor.Red;
            string underline = lineText.Replace(arg.Trim(), new string('^', arg.Trim().Length));
            underline = new String(underline.Select(r => r != '^' ? ' ' : '^').ToArray());
            string pl = "  " + (line + 1) + ": ";
            Console.WriteLine(
                "ERROR:\r\n" +
                "\r\n" + pl + lineText + " <-- [" + message + "] \r\n" +
                new string(' ', pl.Length) + underline
                );
            if (messageLate != null)
                Console.WriteLine(messageLate);
            Console.ResetColor();
            return;
        }
        static string GetStringFromArgument(string w) => w.Substring(w.IndexOf(':') + 1).Trim();
    }
}
