﻿/*
    ISC-No-Nuclear License

    Copyright (c) 2022 - 2023 Wiktor Cielebon

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.

    You acknowledge that this software is not designed, licensed or intended for 
    use in the design, construction, operation or maintenance of any nuclear facility.

 */

using GVM.Assembler;
using System;
using System.Collections.Generic;

namespace GVM {
    public class Machine {
        public Machine(IArchitecture arch, ulong nMaxBytes = 1024 * 1024) {
            foreach (var X in arch.Instructions) {
                this.instructions.Add(X.Opcode, X);
            }
            Memory = new Memory(nMaxBytes);
            Memory.Owner = this;
        }
        private Dictionary<byte, Instruction> instructions = new Dictionary<byte, Instruction>();
        public Memory Memory;

        public Stack<uint> R0  = new Stack<uint>();   // 0x00
        public Stack<uint> R1  = new Stack<uint>();   // 0x01
        public Stack<uint> R2  = new Stack<uint>();   // 0x02
        public Stack<uint> R3  = new Stack<uint>();   // 0x03
        public Stack<uint> R4  = new Stack<uint>();   // 0x04
        public Stack<uint> R5  = new Stack<uint>();   // 0x05
        public Stack<uint> R6  = new Stack<uint>();   // 0x06
        public Stack<uint> R7  = new Stack<uint>();   // 0x07
        public Stack<uint> R8  = new Stack<uint>();   // 0x08
        public Stack<uint> R9  = new Stack<uint>();   // 0x09
        public Stack<uint> R10 = new Stack<uint>();  // 0x0A
        public Stack<uint> R11 = new Stack<uint>();  // 0x0B
        public Stack<uint> R12 = new Stack<uint>();  // 0x0C
        public Stack<uint> R13 = new Stack<uint>();  // 0x0D
        public Stack<uint> R14 = new Stack<uint>();  // 0x0E
        public Stack<uint> R15 = new Stack<uint>();  // 0x0F

        public Stack<ulong> RX0  = new Stack<ulong>();  // 0x10
        public Stack<ulong> RX1  = new Stack<ulong>();  // 0x11
        public Stack<ulong> RX2  = new Stack<ulong>();  // 0x12
        public Stack<ulong> RX3  = new Stack<ulong>();  // 0x13
        public Stack<ulong> RX4  = new Stack<ulong>();  // 0x14
        public Stack<ulong> RX5  = new Stack<ulong>();  // 0x15
        public Stack<ulong> RX6  = new Stack<ulong>();  // 0x16
        public Stack<ulong> RX7  = new Stack<ulong>();  // 0x17
        public Stack<ulong> RX8  = new Stack<ulong>();  // 0x18
        public Stack<ulong> RX9  = new Stack<ulong>();  // 0x19
        public Stack<ulong> RX10 = new Stack<ulong>(); // 0x1A
        public Stack<ulong> RX11 = new Stack<ulong>(); // 0x1B
        public Stack<ulong> RX12 = new Stack<ulong>(); // 0x1C
        public Stack<ulong> RX13 = new Stack<ulong>(); // 0x1D
        public Stack<ulong> RX14 = new Stack<ulong>(); // 0x1E
        public Stack<ulong> RX15 = new Stack<ulong>(); // 0x1F

        public Stack<float> V0  = new Stack<float>();   // 0x20
        public Stack<float> V1  = new Stack<float>();   // 0x21
        public Stack<float> V2  = new Stack<float>();   // 0x22
        public Stack<float> V3  = new Stack<float>();   // 0x23
        public Stack<float> V4  = new Stack<float>();   // 0x24
        public Stack<float> V5  = new Stack<float>();   // 0x25
        public Stack<float> V6  = new Stack<float>();   // 0x26
        public Stack<float> V7  = new Stack<float>();   // 0x27
        public Stack<float> V8  = new Stack<float>();   // 0x28
        public Stack<float> V9  = new Stack<float>();   // 0x29
        public Stack<float> V10 = new Stack<float>();  // 0x2A
        public Stack<float> V11 = new Stack<float>();  // 0x2B
        public Stack<float> V12 = new Stack<float>();  // 0x2C
        public Stack<float> V13 = new Stack<float>();  // 0x2D
        public Stack<float> V14 = new Stack<float>();  // 0x2E
        public Stack<float> V15 = new Stack<float>();  // 0x2F

        public Stack<ulong> PC = new Stack<ulong>();   // 0x30
        public byte CF;    // 0x31
        public bool C;     // 0x32

        public bool RaiseDisassembly { get; set; } = false;

        public ulong Origin;
        public ulong OriginStart;
        public void Execute(byte[] bytecode) {
            for (ulong I = 0; I < (ulong)bytecode.Length; I++) Memory[I] = bytecode[I];
            for (byte I = 0; I <= 0x30; I++) PushRegister(I);
            for (; PC.Peek() < (ulong)bytecode.Length;) {

                PC0Fix:
                //
                // Note that we're using try .. catch here. This is intentional as using try .. catch instead of Dictionary.ContainsKey is faster should the function rarely fail
                // which is the case here.
                //
                try {

                    var instr = instructions[Memory[PC.Peek()]];

                    var len = instr.Length;
                    var para = new byte[len];
                    for (ulong J = 0; J < len; J++) {
                        para[J] = Memory[PC.Peek() + J + 1];
                    }

                    if (RaiseDisassembly) {
                        Disassembly?.Invoke(this, new DisassemblyEventArgs(
                            Memory[PC.Peek()], 
                            para, 
                            (instr.Mnemonic != null ? instr.Mnemonic : "???"), 
                            (instr.Mnemonic != null ? instr.Mnemonic : "???") + " " + ((instr.Disassembly != null && para != null) ? instr.Disassembly(para) : ""),
                            PC.Peek(),
                            instr));
                    }
                    OriginStart = PC.Peek();
                    SetRegister(0x30, (ulong)GetRegister(0x30) + len);
                    //this.PC += len; // Do not move this below the op action as it will fuck up the JMP/B
                    Origin = PC.Peek();
                    instr.Action(para, this);
                    
                    if (Origin != PC.Peek()) goto PC0Fix;
                    SetRegister(0x30, (ulong)GetRegister(0x30) + 1);
                } catch (KeyNotFoundException) {
                    OpcodeNotFound?.Invoke(this, new OpcodeNotFoundEventArgs(Memory[PC.Peek()]));
                } catch (Exception x) {
                    FatalError?.Invoke(this, new FatalErrorEventArgs(x));
                    return;
                }
            }
        }
        public event EventHandler<OpcodeNotFoundEventArgs> OpcodeNotFound;
        public event EventHandler<DisassemblyEventArgs> Disassembly;

        public event EventHandler<FatalErrorEventArgs> FatalError;

        public void RaiseInterruptEvent(byte nInterruptCode) =>
            Interrupt?.Invoke(this, new InterruptEventArgs() {
                Code = nInterruptCode
            });


        public event EventHandler<InterruptEventArgs> Interrupt;

        public static string GetRegisterMnemonic(byte id) {
            #region switch .. case to get register by ID.
            switch (id) {
                case 0x00:
                    return "R0";
                case 0x01:
                    return "R1";
                case 0x02:
                    return "R2";
                case 0x03:
                    return "R3";
                case 0x04:
                    return "R4";
                case 0x05:
                    return "R5";
                case 0x06:
                    return "R6";
                case 0x07:
                    return "R7";
                case 0x08:
                    return "R8";
                case 0x09:
                    return "R9";
                case 0x0A:
                    return "R10";
                case 0x0B:
                    return "R11";
                case 0x0C:
                    return "R12";
                case 0x0D:
                    return "R13";
                case 0x0E:
                    return "R14";
                case 0x0F:
                    return "R15";
                case 0x10:
                    return "RX0";
                case 0x11:
                    return "RX1";
                case 0x12:
                    return "RX2";
                case 0x13:
                    return "RX3";
                case 0x14:
                    return "RX4";
                case 0x15:
                    return "RX5";
                case 0x16:
                    return "RX6";
                case 0x17:
                    return "RX7";
                case 0x18:
                    return "RX8";
                case 0x19:
                    return "RX9";
                case 0x1A:
                    return "RX10";
                case 0x1B:
                    return "RX11";
                case 0x1C:
                    return "RX12";
                case 0x1D:
                    return "RX13";
                case 0x1E:
                    return "RX14";
                case 0x1F:
                    return "RX15";
                case 0x20:
                    return "V0";
                case 0x21:
                    return "V1";
                case 0x22:
                    return "V2";
                case 0x23:
                    return "V3";
                case 0x24:
                    return "V4";
                case 0x25:
                    return "V5";
                case 0x26:
                    return "V6";
                case 0x27:
                    return "V7";
                case 0x28:
                    return "V8";
                case 0x29:
                    return "V9";
                case 0x2A:
                    return "V10";
                case 0x2B:
                    return "V11";
                case 0x2C:
                    return "V12";
                case 0x2D:
                    return "V13";
                case 0x2E:
                    return "V14";
                case 0x2F:
                    return "V15";
                case 0x30:
                    return "PC";
                case 0x31:
                    return "CF";
                case 0x32:
                    return "C";
                default:
                    return null;
            }
            #endregion
        }

        public static byte GetRegisterID(string mnemonic) {
            #region switch .. case to get register ID by mnemonic.
            switch (mnemonic) {
                case "R0":
                    return 0x00;
                case "R1":
                    return 0x01;
                case "R2":
                    return 0x02;
                case "R3":
                    return 0x03;
                case "R4":
                    return 0x04;
                case "R5":
                    return 0x05;
                case "R6":
                    return 0x06;
                case "R7":
                    return 0x07;
                case "R8":
                    return 0x08;
                case "R9":
                    return 0x09;
                case "R10":
                    return 0x0A;
                case "R11":
                    return 0x0B;
                case "R12":
                    return 0x0C;
                case "R13":
                    return 0x0D;
                case "R14":
                    return 0x0E;
                case "R15":
                    return 0x0F;

                case "RX0":
                    return 0x10;
                case "RX1":
                    return 0x11;
                case "RX2":
                    return 0x12;
                case "RX3":
                    return 0x13;
                case "RX4":
                    return 0x14;
                case "RX5":
                    return 0x15;
                case "RX6":
                    return 0x16;
                case "RX7":
                    return 0x17;
                case "RX8":
                    return 0x18;
                case "RX9":
                    return 0x19;
                case "RX10":
                    return 0x1A;
                case "RX11":
                    return 0x1B;
                case "RX12":
                    return 0x1C;
                case "RX13":
                    return 0x1D;
                case "RX14":
                    return 0x1E;
                case "RX15":
                    return 0x1F;

                case "V0":
                    return 0x20;
                case "V1":
                    return 0x21;
                case "V2":
                    return 0x22;
                case "V3":
                    return 0x23;
                case "V4":
                    return 0x24;
                case "V5":
                    return 0x25;
                case "V6":
                    return 0x26;
                case "V7":
                    return 0x27;
                case "V8":
                    return 0x28;
                case "V9":
                    return 0x29;
                case "V10":
                    return 0x2A;
                case "V11":
                    return 0x2B;
                case "V12":
                    return 0x2C;
                case "V13":
                    return 0x2D;
                case "V14":
                    return 0x2E;
                case "V15":
                    return 0x2F;

                case "PC":
                    return 0x30;
                case "CF":
                    return 0x31;
                case "C":
                    return 0x32;
                default:
                    return 0xFF;
            }
            #endregion
        }

        public ValueType GetRegister(byte id) {
            #region switch .. case to get register by ID.
            switch (id) {
                case 0x00:
                    return R0.Peek();
                case 0x01:
                    return R1.Peek();
                case 0x02:
                    return R2.Peek();
                case 0x03:
                    return R3.Peek();
                case 0x04:
                    return R4.Peek();
                case 0x05:
                    return R5.Peek();
                case 0x06:
                    return R6.Peek();
                case 0x07:
                    return R7.Peek();
                case 0x08:
                    return R8.Peek();
                case 0x09:
                    return R9.Peek();
                case 0x0A:
                    return R10.Peek();
                case 0x0B:
                    return R11.Peek();
                case 0x0C:
                    return R12.Peek();
                case 0x0D:
                    return R13.Peek();
                case 0x0E:
                    return R14.Peek();
                case 0x0F:
                    return R15.Peek();
                case 0x10:
                    return RX0.Peek();
                case 0x11:
                    return RX1.Peek();
                case 0x12:
                    return RX2.Peek();
                case 0x13:
                    return RX3.Peek();
                case 0x14:
                    return RX4.Peek();
                case 0x15:
                    return RX5.Peek();
                case 0x16:
                    return RX6.Peek();
                case 0x17:
                    return RX7.Peek();
                case 0x18:
                    return RX8.Peek();
                case 0x19:
                    return RX9.Peek();
                case 0x1A:
                    return RX10.Peek();
                case 0x1B:
                    return RX11.Peek();
                case 0x1C:
                    return RX12.Peek();
                case 0x1D:
                    return RX13.Peek();
                case 0x1E:
                    return RX14.Peek();
                case 0x1F:
                    return RX15.Peek();
                case 0x20:
                    return V0.Peek();
                case 0x21:
                    return V1.Peek();
                case 0x22:
                    return V2.Peek();
                case 0x23:
                    return V3.Peek();
                case 0x24:
                    return V4.Peek();
                case 0x25:
                    return V5.Peek();
                case 0x26:
                    return V6.Peek();
                case 0x27:
                    return V7.Peek();
                case 0x28:
                    return V8.Peek();
                case 0x29:
                    return V9.Peek();
                case 0x2A:
                    return V10.Peek();
                case 0x2B:
                    return V11.Peek();
                case 0x2C:
                    return V12.Peek();
                case 0x2D:
                    return V13.Peek();
                case 0x2E:
                    return V14.Peek();
                case 0x2F:
                    return V15.Peek();
                case 0x30:
                    return PC.Peek();
                case 0x31:
                    return CF;
                case 0x32:
                    return C;
                default:
                    return null;
            }
            #endregion
        }

        public void SetRegister(byte id, ValueType value) {
            #region switch .. case to set register by ID.
            switch (id) {
                case 0x00:
                    R0.Pop();
                    R0.Push((uint)value);
                    break;
                case 0x01:
                    R1.Pop();
                    R1.Push((uint)value);
                    break;
                case 0x02:
                    R2.Pop();
                    R2.Push((uint)value);
                    break;
                case 0x03:
                    R3.Pop();
                    R3.Push((uint)value);
                    break;
                case 0x04:
                    R4.Pop();
                    R4.Push((uint)value);
                    break;
                case 0x05:
                    R5.Pop();
                    R5.Push((uint)value);
                    break;
                case 0x06:
                    R6.Pop();
                    R6.Push((uint)value);
                    break;
                case 0x07:
                    R7.Pop();
                    R7.Push((uint)value);
                    break;
                case 0x08:
                    R8.Pop();
                    R8.Push((uint)value);
                    break;
                case 0x09:
                    R9.Pop();
                    R9.Push((uint)value);
                    break;
                case 0x0A:
                    R10.Pop();
                    R10.Push((uint)value);
                    break;
                case 0x0B:
                    R11.Pop();
                    R11.Push((uint)value);
                    break;
                case 0x0C:
                    R12.Pop();
                    R12.Push((uint)value);
                    break;
                case 0x0D:
                    R13.Pop();
                    R13.Push((uint)value);
                    break;
                case 0x0E:
                    R14.Pop();
                    R14.Push((uint)value);
                    break;
                case 0x0F:
                    R15.Pop();
                    R15.Push((uint)value);
                    break;
                case 0x10:
                    RX0.Pop();
                    RX0.Push((ulong)value);
                    break;
                case 0x11:
                    RX1.Pop();
                    RX1.Push((ulong)value);
                    break;
                case 0x12:
                    RX2.Pop();
                    RX2.Push((ulong)value);
                    break;
                case 0x13:
                    RX3.Pop();
                    RX3.Push((ulong)value);
                    break;
                case 0x14:
                    RX4.Pop();
                    RX4.Push((ulong)value);
                    break;
                case 0x15:
                    RX5.Pop();
                    RX5.Push((ulong)value);
                    break;
                case 0x16:
                    RX6.Pop();
                    RX6.Push((ulong)value);
                    break;
                case 0x17:
                    RX7.Pop();
                    RX7.Push((ulong)value);
                    break;
                case 0x18:
                    RX8.Pop();
                    RX8.Push((ulong)value);
                    break;
                case 0x19:
                    RX9.Pop();
                    RX9.Push((ulong)value);
                    break;
                case 0x1A:
                    RX10.Pop();
                    RX10.Push((ulong)value);
                    break;
                case 0x1B:
                    RX11.Pop();
                    RX11.Push((ulong)value);
                    break;
                case 0x1C:
                    RX12.Pop();
                    RX12.Push((ulong)value);
                    break;
                case 0x1D:
                    RX13.Pop();
                    RX13.Push((ulong)value);
                    break;
                case 0x1E:
                    RX14.Pop();
                    RX14.Push((ulong)value);
                    break;
                case 0x1F:
                    RX15.Pop();
                    RX15.Push((ulong)value);
                    break;
                case 0x20:
                    V0.Pop();
                    V0.Push((float)value);
                    break;
                case 0x21:
                    V1.Pop();
                    V1.Push((float)value);
                    break;
                case 0x22:
                    V2.Pop();
                    V2.Push((float)value);
                    break;
                case 0x23:
                    V3.Pop();
                    V3.Push((float)value);
                    break;
                case 0x24:
                    V4.Pop();
                    V4.Push((float)value);
                    break;
                case 0x25:
                    V5.Pop();
                    V5.Push((float)value);
                    break;
                case 0x26:
                    V6.Pop();
                    V6.Push((float)value);
                    break;
                case 0x27:
                    V7.Pop();
                    V7.Push((float)value);
                    break;
                case 0x28:
                    V8.Pop();
                    V8.Push((float)value);
                    break;
                case 0x29:
                    V9.Pop();
                    V9.Push((float)value);
                    break;
                case 0x2A:
                    V10.Pop();
                    V10.Push((float)value);
                    break;
                case 0x2B:
                    V11.Pop();
                    V11.Push((float)value);
                    break;
                case 0x2C:
                    V12.Pop();
                    V12.Push((float)value);
                    break;
                case 0x2D:
                    V13.Pop();
                    V13.Push((float)value);
                    break;
                case 0x2E:
                    V14.Pop();
                    V14.Push((float)value);
                    break;
                case 0x2F:
                    V15.Pop();
                    V15.Push((float)value);
                    break;
                case 0x30:
                    PC.Pop();
                    PC.Push((ulong)value);
                    break;
                case 0x31:
                    CF = (byte)value;
                    break;
                case 0x32:
                    C = (bool)value;
                    break;
                default:
                    break;
            }
            #endregion
        }

        public void PushRegister(byte id) {
            // Disassembly?.Invoke(this, new DisassemblyEventArgs(0x00, new byte[] { 0x00 }, "Push " + id.ToString("X2"), "Push " + id.ToString("X2"), 0x00, new Instruction()));
            #region switch .. case to get register by ID.
            switch (id) {
                case 0x00:
                    R0.Push(0);
                    break;
                case 0x01:
                    R1.Push(0);
                    break;
                case 0x02:
                    R2.Push(0);
                    break;
                case 0x03:
                    R3.Push(0);
                    break;
                case 0x04:
                    R4.Push(0);
                    break;
                case 0x05:
                    R5.Push(0);
                    break;
                case 0x06:
                    R6.Push(0);
                    break;
                case 0x07:
                    R7.Push(0);
                    break;
                case 0x08:
                    R8.Push(0);
                    break;
                case 0x09:
                    R9.Push(0);
                    break;
                case 0x0A:
                    R10.Push(0);
                    break;
                case 0x0B:
                    R11.Push(0);
                    break;
                case 0x0C:
                    R12.Push(0);
                    break;
                case 0x0D:
                    R13.Push(0);
                    break;
                case 0x0E:
                    R14.Push(0);
                    break;
                case 0x0F:
                    R15.Push(0);
                    break;
                case 0x10:
                    RX0.Push(0);
                    break;
                case 0x11:
                    RX1.Push(0);
                    break;
                case 0x12:
                    RX2.Push(0);
                    break;
                case 0x13:
                    RX3.Push(0);
                    break;
                case 0x14:
                    RX4.Push(0);
                    break;
                case 0x15:
                    RX5.Push(0);
                    break;
                case 0x16:
                    RX6.Push(0);
                    break;
                case 0x17:
                    RX7.Push(0);
                    break;
                case 0x18:
                    RX8.Push(0);
                    break;
                case 0x19:
                    RX9.Push(0);
                    break;
                case 0x1A:
                    RX10.Push(0);
                    break;
                case 0x1B:
                    RX11.Push(0);
                    break;
                case 0x1C:
                    RX12.Push(0);
                    break;
                case 0x1D:
                    RX13.Push(0);
                    break;
                case 0x1E:
                    RX14.Push(0);
                    break;
                case 0x1F:
                    RX15.Push(0);
                    break;
                case 0x20:
                    V0.Push(0F);
                    break;
                case 0x21:
                    V1.Push(0F);
                    break;
                case 0x22:
                    V2.Push(0F);
                    break;
                case 0x23:
                    V3.Push(0F);
                    break;
                case 0x24:
                    V4.Push(0F);
                    break;
                case 0x25:
                    V5.Push(0F);
                    break;
                case 0x26:
                    V6.Push(0F);
                    break;
                case 0x27:
                    V7.Push(0F);
                    break;
                case 0x28:
                    V8.Push(0F);
                    break;
                case 0x29:
                    V9.Push(0F);
                    break;
                case 0x2A:
                    V10.Push(0F);
                    break;
                case 0x2B:
                    V11.Push(0F);
                    break;
                case 0x2C:
                    V12.Push(0F);
                    break;
                case 0x2D:
                    V13.Push(0F);
                    break;
                case 0x2E:
                    V14.Push(0F);
                    break;
                case 0x2F:
                    V15.Push(0F);
                    break;
                case 0x30:
                    PC.Push(0);
                    break;
                default:
                    return;
            }
            #endregion
        }
        public void PopRegister(byte id) {
            #region switch .. case to get register by ID.
            switch (id) {
                case 0x00:
                    R0.Pop(); break;
                case 0x01:
                    R1.Pop(); break;
                case 0x02:
                    R2.Pop(); break;
                case 0x03:
                    R3.Pop(); break;
                case 0x04:
                    R4.Pop(); break;
                case 0x05:
                    R5.Pop(); break;
                case 0x06:
                    R6.Pop(); break;
                case 0x07:
                    R7.Pop(); break;
                case 0x08:
                    R8.Pop(); break;
                case 0x09:
                    R9.Pop(); break;
                case 0x0A:
                    R10.Pop(); break;
                case 0x0B:
                    R11.Pop(); break;
                case 0x0C:
                    R12.Pop(); break;
                case 0x0D:
                    R13.Pop(); break;
                case 0x0E:
                    R14.Pop(); break;
                case 0x0F:
                    R15.Pop(); break;
                case 0x10:
                    RX0.Pop(); break;
                case 0x11:
                    RX1.Pop(); break;
                case 0x12:
                    RX2.Pop(); break;
                case 0x13:
                    RX3.Pop(); break;
                case 0x14:
                    RX4.Pop(); break;
                case 0x15:
                    RX5.Pop(); break;
                case 0x16:
                    RX6.Pop(); break;
                case 0x17:
                    RX7.Pop(); break;
                case 0x18:
                    RX8.Pop(); break;
                case 0x19:
                    RX9.Pop(); break;
                case 0x1A:
                    RX10.Pop(); break;
                case 0x1B:
                    RX11.Pop(); break;
                case 0x1C:
                    RX12.Pop(); break;
                case 0x1D:
                    RX13.Pop(); break;
                case 0x1E:
                    RX14.Pop(); break;
                case 0x1F:
                    RX15.Pop(); break;
                case 0x20:
                    V0.Pop(); break;
                case 0x21:
                    V1.Pop(); break;
                case 0x22:
                    V2.Pop(); break;
                case 0x23:
                    V3.Pop(); break;
                case 0x24:
                    V4.Pop(); break;
                case 0x25:
                    V5.Pop(); break;
                case 0x26:
                    V6.Pop(); break;
                case 0x27:
                    V7.Pop(); break;
                case 0x28:
                    V8.Pop(); break;
                case 0x29:
                    V9.Pop(); break;
                case 0x2A:
                    V10.Pop(); break;
                case 0x2B:
                    V11.Pop(); break;
                case 0x2C:
                    V12.Pop(); break;
                case 0x2D:
                    V13.Pop(); break;
                case 0x2E:
                    V14.Pop(); break;
                case 0x2F:
                    V15.Pop(); break;
                case 0x30:
                    PC.Pop(); break;
                default:
                    break;
            }
            #endregion
        }
        //
        // Not available in .NET Standard, but I'm leaving the function here so you can just copy-paste it into your code.
        //
        //public static IArchitecture GetArchitectureFromFile(string Filename) {
        //    if (System.IO.File.Exists(Filename)) {
        //        Type T = typeof(IArchitecture);
        //        System.Reflection.Assembly a = System.Reflection.Assembly.LoadFrom(Filename);
        //        foreach (Type TP in a.GetTypes()) {
        //            if (TP.IsNotPublic)
        //                continue;
        //            if (T.IsAssignableFrom(TP)) {
        //                IArchitecture arch = (IArchitecture)Activator.CreateInstance(TP);
        //                return arch;
        //            }
        //        }
        //    }
        //    return null;
        //}
    }
}
