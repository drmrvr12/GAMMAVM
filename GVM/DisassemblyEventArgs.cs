﻿/*
    ISC-No-Nuclear License

    Copyright (c) 2022 Wiktor Cielebon

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.

    You acknowledge that this software is not designed, licensed or intended for 
    use in the design, construction, operation or maintenance of any nuclear facility.

 */

using GVM.Assembler;
using System;

namespace GVM {
    public class DisassemblyEventArgs : EventArgs {
        public DisassemblyEventArgs(Byte opcode, Byte[] args, String mnemonic, String disassembly, UInt64 pc, Instruction instruction) {
            this.Opcode = opcode;
            this.Args = args;
            this.Mnemonic = mnemonic;
            this.Disassembly = disassembly;
            this.PC = pc;
            this.Instruction = instruction;
        }

        public byte Opcode { get; private set; }
        public byte[] Args { get; private set; }
        public string Mnemonic { get; private set; }
        public string Disassembly { get; private set; }
        public ulong PC { get; private set; }
        public Instruction Instruction { get; private set; }
    }
}
