﻿/*
    ISC-No-Nuclear License

    Copyright (c) 2022 Wiktor Cielebon

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.

    You acknowledge that this software is not designed, licensed or intended for 
    use in the design, construction, operation or maintenance of any nuclear facility.

 */

using System;

namespace GVM {
    public class Memory {
        public Memory(ulong nMaxBytes, uint nInitialBlocksAllocated = 1, uint nBlockSize = 1024) {
            memory = new byte[nInitialBlocksAllocated + 1][];
            BlocksAllocated = nInitialBlocksAllocated;
            BlockSize = nBlockSize;
            MaxBytes = nMaxBytes;
        }
        public Machine Owner { get; set; }
        public void Expand(uint nBlocks) {
            byte[][] memoryNew = new byte[BlocksAllocated + nBlocks + 1][];

            MemoryExpanded?.Invoke(this, new MemoryExpandedEventArgs(memory.Length, memoryNew.Length));

            for (ulong I = 0; I <= BlocksAllocated; I++) {
                memoryNew[I] = memory[I];
            }
            for (ulong I = BlocksAllocated; I < BlocksAllocated + nBlocks; I++) {
                memoryNew[I] = new byte[BlockSize];
            }

            BlocksAllocated += nBlocks;
            memory = memoryNew;
        }
        public byte[] GetRange(ulong addr, uint count) {
            byte[] ret = new byte[count + 1];
            for (ulong I = addr; I <= addr + count; I++) {
                ret[I - addr] = this[I];
            }
            return ret;
        }
        public byte this[ulong addr] {
            get {
                if ((ulong)memory.Length * BlockSize < addr) {
                    throw new Exception("The instruction at " + (Owner != null ? "0x" + Owner.OriginStart.ToString("X16") : "?") + " referenced virtual memory at 0x" + (addr).ToString("X16") + ", the memory address is out of range of the currently allocated memory.");
                }
                var m = memory[addr / BlockSize];
                return m!=null?m[addr % BlockSize]:(byte)0x00;
            }
            set {
                Retry:
                if (BlocksAllocated * BlockSize < addr + 1) {
                    if (BlocksAllocated * BlockSize > MaxBytes)
                        throw new Exception("Attempted to expand virtual memory outside of the bounds defined by the nMaxBytes parameter.");
                    Expand(16);
                    goto Retry;
                }
                var m = memory[addr / BlockSize];
                if (m == null) { // Nasty hack to fix a nullref but it works
                    memory[addr / BlockSize] = new byte[BlockSize];
                    m = memory[addr / BlockSize];
                }
                m[addr % BlockSize] = value;
            }
        }
        public ulong BlocksAllocated { get; private set; }
        public ulong MaxBytes { get; set; }
        private byte[][] memory;
        public uint BlockSize { get; private set; }
        public event EventHandler<MemoryExpandedEventArgs> MemoryExpanded;

    }
}
