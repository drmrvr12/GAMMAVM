﻿/*
 * Copyright (C) 2022 - 2023 Wiktor Cielebon
 * 
 * Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO 
 * THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO 
 * EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL 
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, 
 * WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING 
 * OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 * 
 */

using GVM;
using GVM.Assembler;
using System;

namespace GAMMA64 {
    public partial class StdGAMMA64 : IArchitecture {
        public String Name => "GAMMA64";
        public Byte Architecture => 64;
        public Instruction[] Instructions => new[] {
            new Instruction("NOP", 0x00, 0x00000000, 0, null, (p,m) => { }),

            new Instruction("JMP", 0x01, 0x00000008, 1, new[] { typeof(ulong) }, (p,m) => {
                m.SetRegister(0x30, BitConverter.ToUInt64(p,0));
            }, new OperandType[] { OperandType.QWORD }, x => "0x" + BitConverter.ToUInt64(x,0).ToString("X16")),

            new Instruction("INT", 0x02, 0x00000001, 1, new[] { typeof(byte) }, (p,m) => {
                m.RaiseInterruptEvent(p[0]);
            }, new OperandType[] { OperandType.BYTE }, x => "0x" + x[0].ToString("X2")),

            new Instruction("MOVQ", 0x03, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], BitConverter.ToUInt64(p,1));
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x=> Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("MOVR", 0x04, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                m.SetRegister(p[0], m.GetRegister(p[1]));
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x=> Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("LEAQ", 0x05, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], BitConverter.ToUInt64(m.Memory.GetRange(BitConverter.ToUInt64(p, 1), 8),0));
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("LEAD", 0x06, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], BitConverter.ToUInt32(m.Memory.GetRange(BitConverter.ToUInt64(p, 1), 4),0));
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("LEAW", 0x07, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], (uint)BitConverter.ToUInt16(m.Memory.GetRange(BitConverter.ToUInt64(p, 1), 2),0));
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("LEAB", 0x08, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.Memory[BitConverter.ToUInt64(p, 1)]);
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("LEAS", 0x09, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], BitConverter.ToSingle(m.Memory.GetRange(BitConverter.ToUInt64(p, 1), 4),0));
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("ADDR", 0x0A, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = m.GetRegister(p[1]);
                if (p0 is ulong) {
                    p0 = (ulong)p0 + (ulong)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is uint) {
                    p0 = (uint)p0 + (uint)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is float) {
                    p0 = (float)p0 + (float)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("ADDQ", 0x0B, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], (ulong)m.GetRegister(p[0]) + BitConverter.ToUInt64(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("ADDD", 0x0C, 0x00000005, 2, new[] { typeof(byte), typeof(uint) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) + BitConverter.ToUInt32(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.DWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt32(x,1).ToString("X8")),

            new Instruction("ADDW", 0x0D, 0x00000003, 2, new[] { typeof(byte), typeof(ushort) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) + BitConverter.ToUInt16(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.WORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt16(x,1).ToString("X4")),

            new Instruction("ADDB", 0x0E, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) + p[1]);
            }, new OperandType[] { OperandType.Register, OperandType.BYTE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + x[1].ToString("X2")),

            new Instruction("ADDS", 0x0F, 0x00000005, 2, new[] { typeof(byte), typeof(float) }, (p,m) => {
                m.SetRegister(p[0], (ulong)m.GetRegister(p[0]) + BitConverter.ToSingle(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.SINGLE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToSingle(x,1)),

            new Instruction("INC", 0x10, 0x00000001, 1, new[] { typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                if (p0 is ulong) {
                    p0 = (ulong)p0 + 1;
                    m.SetRegister(p[0], p0);
                    return;
                }
                if (p0 is uint) {
                    p0 = (uint)p0 + 1;
                    m.SetRegister(p[0], p0);
                    return;
                }
            }, new OperandType[] { OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0])),
            new Instruction("DEC", 0x11, 0x00000001, 1, new[] { typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                if (p0 is ulong) {
                    p0 = (ulong)p0 - 1;
                    m.SetRegister(p[0], p0);
                    return;
                }
                if (p0 is uint) {
                    p0 = (uint)p0 - 1;
                    m.SetRegister(p[0], p0);
                    return;
                }
            }, new OperandType[] { OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0])),




            new Instruction("CMPR", 0x12, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var a = m.GetRegister(p[0]);
                var b = m.GetRegister(p[1]);
                if (a is ulong) {
                    ulong ax = (ulong)a;
                    ulong bx = (ulong)b;
                    m.CF = 0;
                    if (bx == ax) {
                        m.CF = 1;
                    } else if (bx < ax) {
                        m.CF = 2;
                    } else if (bx > ax) {
                        m.CF = 3;
                    }
                }
                if (a is uint) {
                    uint ax = (uint)a;
                    uint bx = (uint)b;
                    m.CF = 0;
                    if (bx == ax) {
                        m.CF = 1;
                    } else if (bx < ax) {
                        m.CF = 2;
                    } else if (bx > ax) {
                        m.CF = 3;
                    }
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("CMPQ", 0x13, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                object gr = m.GetRegister(p[0]);
                ulong p0x = gr is ulong? (ulong)gr:((uint)gr);
                ulong p1x = BitConverter.ToUInt64(p, 1);
                m.CF = 0;
                if (p1x == p0x) {
                    m.CF = 1;
                } else if (p1x < p0x) {
                    m.CF = 2;
                } else if (p1x > p0x) {
                    m.CF = 3;
                }
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("CMPD", 0x14, 0x00000005, 2, new[] { typeof(byte), typeof(uint) }, (p,m) => {
                object gr = m.GetRegister(p[0]);
                ulong p0x = gr is ulong? (ulong)gr:((uint)gr);
                uint p1x = BitConverter.ToUInt32(p, 1);
                m.CF = 0;
                if (p1x == p0x) {
                    m.CF = 1;
                } else if (p1x < p0x) {
                    m.CF = 2;
                } else if (p1x > p0x) {
                    m.CF = 3;
                }
            }, new OperandType[] { OperandType.Register, OperandType.DWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt32(x,1).ToString("X8")),

            new Instruction("CMPW", 0x15, 0x00000003, 2, new[] { typeof(byte), typeof(ushort) }, (p,m) => {
                object gr = m.GetRegister(p[0]);
                ulong p0x = gr is ulong? (ulong)gr:((uint)gr);
                ushort p1x = BitConverter.ToUInt16(p, 1);
                m.CF = 0;
                if (p1x == p0x) {
                    m.CF = 1;
                } else if (p1x < p0x) {
                    m.CF = 2;
                } else if (p1x > p0x) {
                    m.CF = 3;
                }
            }, new OperandType[] { OperandType.Register, OperandType.WORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt16(x,1).ToString("X4")),

            new Instruction("CMPB", 0x16, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                object gr = m.GetRegister(p[0]);
                ulong p0x = gr is ulong? (ulong)gr:((uint)gr);
                
                byte p1x = p[1];
                m.CF = 0;
                if (p1x == p0x) {
                    m.CF = 1;
                } else if (p1x < p0x) {
                    m.CF = 2;
                } else if (p1x > p0x) {
                    m.CF = 3;
                }
            }, new OperandType[] { OperandType.Register, OperandType.BYTE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + x[1].ToString("X2")),

            new Instruction("JNE", 0x17, 0x00000008, 1, new[] { typeof(ulong) }, (p,m) => {
                if (m.CF != 1) m.SetRegister(0x30, BitConverter.ToUInt64(p,0));
            }, new OperandType[] { OperandType.QWORD }, x => "0x" + BitConverter.ToUInt64(x,0).ToString("X16")),
            new Instruction("JE", 0x18, 0x00000008, 1, new[] { typeof(ulong) }, (p,m) => {
                if (m.CF == 1) m.SetRegister(0x30, BitConverter.ToUInt64(p,0));
            }, new OperandType[] { OperandType.QWORD }, x => "0x" + BitConverter.ToUInt64(x,0).ToString("X16")),
            new Instruction("JG", 0x19, 0x00000008, 1, new[] { typeof(ulong) }, (p,m) => {
                if (m.CF == 2) m.SetRegister(0x30, BitConverter.ToUInt64(p,0));
            }, new OperandType[] { OperandType.QWORD }, x => "0x" + BitConverter.ToUInt64(x,0).ToString("X16")),
            new Instruction("JL", 0x1A, 0x00000008, 1, new[] { typeof(ulong) }, (p,m) => {
                if (m.CF == 3) m.SetRegister(0x30, BitConverter.ToUInt64(p,0));
            }, new OperandType[] { OperandType.QWORD }, x => "0x" + BitConverter.ToUInt64(x,0).ToString("X16")),
            new Instruction("JGE", 0x1B, 0x00000008, 1, new[] { typeof(ulong) }, (p,m) => {
                if (m.CF == 1 || m.CF == 2) m.SetRegister(0x30, BitConverter.ToUInt64(p,0));
            }, new OperandType[] { OperandType.QWORD }, x => "0x" + BitConverter.ToUInt64(x,0).ToString("X16")),
            new Instruction("JLE", 0x1C, 0x00000008, 1, new[] { typeof(ulong) }, (p,m) => {
                if (m.CF == 1 || m.CF == 3) m.SetRegister(0x30, BitConverter.ToUInt64(p,0));
            }, new OperandType[] { OperandType.QWORD }, x => "0x" + BitConverter.ToUInt64(x,0).ToString("X16")),

            new Instruction("MOVD", 0x1D, 0x00000005, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], BitConverter.ToUInt32(p,1));
            }, new OperandType[] { OperandType.Register, OperandType.DWORD }, x=> Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt32(x,1).ToString("X8")),
            new Instruction("MOVW", 0x1E, 0x00000005, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], BitConverter.ToUInt16(p,1));
            }, new OperandType[] { OperandType.Register, OperandType.WORD }, x=> Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt16(x,1).ToString("X4")),
            new Instruction("MOVB", 0x1F, 0x00000002, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], p[1]);
            }, new OperandType[] { OperandType.Register, OperandType.BYTE }, x=> Machine.GetRegisterMnemonic(x[0]) + ", 0x" + x[1].ToString("X2")),
            new Instruction("MOVS", 0x20, 0x00000005, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], BitConverter.ToSingle(p,1));
            }, new OperandType[] { OperandType.Register, OperandType.SINGLE }, x=> Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToSingle(x,1).ToString()),
            new Instruction("SUBR", 0x21, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = m.GetRegister(p[1]);
                if (p0 is ulong) {
                    p0 = (ulong)p0 - (ulong)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is uint) {
                    p0 = (uint)p0 - (uint)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is float) {
                    p0 = (float)p0 - (float)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("SUBQ", 0x22, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], (ulong)m.GetRegister(p[0]) - BitConverter.ToUInt64(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("SUBD", 0x23, 0x00000005, 2, new[] { typeof(byte), typeof(uint) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) - BitConverter.ToUInt32(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.DWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt32(x,1).ToString("X8")),

            new Instruction("SUBW", 0x24, 0x00000003, 2, new[] { typeof(byte), typeof(ushort) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) - BitConverter.ToUInt16(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.WORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt16(x,1).ToString("X4")),

            new Instruction("SUBB", 0x25, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) - p[1]);
            }, new OperandType[] { OperandType.Register, OperandType.BYTE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + x[1].ToString("X2")),

            new Instruction("SUBS", 0x26, 0x00000005, 2, new[] { typeof(byte), typeof(float) }, (p,m) => {
                m.SetRegister(p[0], (ulong)m.GetRegister(p[0]) - BitConverter.ToSingle(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.SINGLE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToSingle(x,1)),
            new Instruction("MULR", 0x27, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = m.GetRegister(p[1]);
                if (p0 is ulong) {
                    p0 = (ulong)p0 * (ulong)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is uint) {
                    p0 = (uint)p0 * (uint)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is float) {
                    p0 = (float)p0 * (float)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("MULQ", 0x28, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], (ulong)m.GetRegister(p[0]) * BitConverter.ToUInt64(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("MULD", 0x29, 0x00000005, 2, new[] { typeof(byte), typeof(uint) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) * BitConverter.ToUInt32(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.DWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt32(x,1).ToString("X8")),

            new Instruction("MULW", 0x2A, 0x00000003, 2, new[] { typeof(byte), typeof(ushort) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) * BitConverter.ToUInt16(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.WORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt16(x,1).ToString("X4")),

            new Instruction("MULB", 0x2B, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) * p[1]);
            }, new OperandType[] { OperandType.Register, OperandType.BYTE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + x[1].ToString("X2")),

            new Instruction("MULS", 0x2C, 0x00000005, 2, new[] { typeof(byte), typeof(float) }, (p,m) => {
                m.SetRegister(p[0], (ulong)m.GetRegister(p[0]) * BitConverter.ToSingle(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.SINGLE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToSingle(x,1)),
            new Instruction("DIVR", 0x2D, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = m.GetRegister(p[1]);
                if (p0 is ulong) {
                    p0 = (ulong)p0 / (ulong)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is uint) {
                    p0 = (uint)p0 / (uint)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is float) {
                    p0 = (float)p0 / (float)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("DIVQ", 0x2E, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], (ulong)m.GetRegister(p[0]) / BitConverter.ToUInt64(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("DIVD", 0x2F, 0x00000005, 2, new[] { typeof(byte), typeof(uint) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) / BitConverter.ToUInt32(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.DWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt32(x,1).ToString("X8")),

            new Instruction("DIVW", 0x30, 0x00000003, 2, new[] { typeof(byte), typeof(ushort) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) / BitConverter.ToUInt16(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.WORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt16(x,1).ToString("X4")),

            new Instruction("DIVB", 0x31, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) / p[1]);
            }, new OperandType[] { OperandType.Register, OperandType.BYTE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + x[1].ToString("X2")),

            new Instruction("DIVS", 0x32, 0x00000005, 2, new[] { typeof(byte), typeof(float) }, (p,m) => {
                m.SetRegister(p[0], (ulong)m.GetRegister(p[0]) / BitConverter.ToSingle(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.SINGLE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToSingle(x,1)),
            new Instruction("MODR", 0x33, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = m.GetRegister(p[1]);
                if (p0 is ulong) {
                    p0 = (ulong)p0 % (ulong)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is uint) {
                    p0 = (uint)p0 % (uint)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is float) {
                    p0 = (float)p0 % (float)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("MODQ", 0x34, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], (ulong)m.GetRegister(p[0]) % BitConverter.ToUInt64(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("MODD", 0x35, 0x00000005, 2, new[] { typeof(byte), typeof(uint) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) % BitConverter.ToUInt32(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.DWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt32(x,1).ToString("X8")),

            new Instruction("MODW", 0x36, 0x00000003, 2, new[] { typeof(byte), typeof(ushort) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) % BitConverter.ToUInt16(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.WORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt16(x,1).ToString("X4")),

            new Instruction("MODB", 0x37, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) % p[1]);
            }, new OperandType[] { OperandType.Register, OperandType.BYTE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + x[1].ToString("X2")),

            new Instruction("MODS", 0x38, 0x00000005, 2, new[] { typeof(byte), typeof(float) }, (p,m) => {
                m.SetRegister(p[0], (ulong)m.GetRegister(p[0]) % BitConverter.ToSingle(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.SINGLE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToSingle(x,1)),

            new Instruction("XORR", 0x39, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = m.GetRegister(p[1]);
                if (p0 is ulong) {
                    p0 = (ulong)p0 ^ (ulong)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is uint) {
                    p0 = (uint)p0 ^ (uint)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("XORQ", 0x3A, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], (ulong)m.GetRegister(p[0]) ^ BitConverter.ToUInt64(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("XORD", 0x3B, 0x00000005, 2, new[] { typeof(byte), typeof(uint) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) ^ BitConverter.ToUInt32(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.DWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt32(x,1).ToString("X8")),

            new Instruction("XORW", 0x3C, 0x00000003, 2, new[] { typeof(byte), typeof(ushort) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) ^ BitConverter.ToUInt16(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.WORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt16(x,1).ToString("X4")),

            new Instruction("XORB", 0x3D, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) ^ p[1]);
            }, new OperandType[] { OperandType.Register, OperandType.BYTE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + x[1].ToString("X2")),

            new Instruction("ORR", 0x3E, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = m.GetRegister(p[1]);
                if (p0 is ulong) {
                    p0 = (ulong)p0 | (ulong)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is uint) {
                    p0 = (uint)p0 | (uint)p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("ORQ", 0x3F, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                m.SetRegister(p[0], (ulong)m.GetRegister(p[0]) | BitConverter.ToUInt64(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("ORD", 0x40, 0x00000005, 2, new[] { typeof(byte), typeof(uint) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) | BitConverter.ToUInt32(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.DWORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt32(x,1).ToString("X8")),

            new Instruction("ORW", 0x41, 0x00000003, 2, new[] { typeof(byte), typeof(ushort) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) | BitConverter.ToUInt16(p, 1));
            }, new OperandType[] { OperandType.Register, OperandType.WORD }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + BitConverter.ToUInt16(x,1).ToString("X4")),

            new Instruction("ORB", 0x42, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                m.SetRegister(p[0], (uint)m.GetRegister(p[0]) | p[1]);
            }, new OperandType[] { OperandType.Register, OperandType.BYTE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + x[1].ToString("X2")),

            new Instruction("LSH", 0x43, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = p[1];
                if (p0 is ulong) {
                    p0 = (ulong)p0 << p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is uint) {
                    p0 = (uint)p0 << p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
            }, new OperandType[] { OperandType.Register, OperandType.BYTE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + x[1].ToString("X2")),

            new Instruction("RSH", 0x44, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = p[1];
                if (p0 is ulong) {
                    p0 = (ulong)p0 >> p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is uint) {
                    p0 = (uint)p0 >> p1;
                    m.SetRegister(p[0],  p0);
                    return;
                }
            }, new OperandType[] { OperandType.Register, OperandType.BYTE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + x[1].ToString("X2")),


            new Instruction("LRB", 0x45, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = p[1];
                if (p0 is ulong) {
                    p0 = (ulong)p0 << p1 | (ulong)p0 >> 63;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is uint) {
                    p0 = (uint)p0 << p1 | (uint)p0 >> 31;
                    m.SetRegister(p[0],  p0);
                    return;
                }
            }, new OperandType[] { OperandType.Register, OperandType.BYTE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + x[1].ToString("X2")),

            new Instruction("RRB", 0x46, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = p[1];
                if (p0 is ulong) {
                    p0 = (ulong)p0 >> p1 | (ulong)p0 << 63;
                    m.SetRegister(p[0],  p0);
                    return;
                }
                if (p0 is uint) {
                    p0 = (uint)p0 >> p1 | (uint)p0 << 31;
                    m.SetRegister(p[0],  p0);
                    return;
                }
            }, new OperandType[] { OperandType.Register, OperandType.BYTE }, x => Machine.GetRegisterMnemonic(x[0]) + ", 0x" + x[1].ToString("X2")),

            new Instruction("LEARB", 0x47, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = m.GetRegister(p[1]);
                if (p0 is ulong) {
                    m.SetRegister(p[0],  (ulong)m.Memory[(ulong)p1]);
                    return;
                }
                if (p0 is uint) {
                    m.SetRegister(p[0],  (uint)m.Memory[(ulong)p1]);
                    return;
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("MCPR", 0x48, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                ulong p0 = (ulong)m.GetRegister(p[0]);
                ulong p1 = (ulong)m.GetRegister(p[1]);
                m.Memory[p0] = Convert.ToByte(m.Memory[p1]);
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),
            new Instruction("MCPQ", 0x49, 0x00000009, 2, new[] { typeof(byte), typeof(ulong) }, (p,m) => {
                ulong p0 = (ulong)m.GetRegister(p[0]);
                ulong p1 = BitConverter.ToUInt64(p,1);
                m.Memory[p0] = Convert.ToByte(m.Memory[p1]);
            }, new OperandType[] { OperandType.Register, OperandType.QWORD }, x => Machine.GetRegisterMnemonic(x[0]) +  ", 0x" + BitConverter.ToUInt64(x,1).ToString("X16")),

            new Instruction("STMQ", 0x4A, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                ulong p0 = (ulong)m.GetRegister(p[0]);
                ulong p1 = (ulong)m.GetRegister(p[1]);
                var b = BitConverter.GetBytes(p1);
                for (ulong I = 0; I < (ulong)b.Length; I++){
                    m.Memory[p0+I] = b[I];
			    }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),
            new Instruction("STMD", 0x4B, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                ulong p0 = (ulong)m.GetRegister(p[0]);
                uint p1 = (uint)m.GetRegister(p[1]);
                var b = BitConverter.GetBytes(p1);
                for (ulong I = 0; I < (ulong)b.Length; I++){
                    m.Memory[p0+I] = b[I];
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("STMB", 0x4C, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                ulong p0 = (ulong)m.GetRegister(p[0]);
                object gr = m.GetRegister(p[1]);
                ulong p1 = gr is ulong? (ulong)gr:((uint)gr);
                var b = Convert.ToByte(p1);
                m.Memory[p0] = b;
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),



            new Instruction("LEARQ", 0x4D, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = m.GetRegister(p[1]);
                if (p0 is ulong) {
                    m.SetRegister(p[0], BitConverter.ToUInt64(m.Memory.GetRange((ulong)p1, 8), 0));
                    return;
                }
                if (p0 is uint) {
                    throw new Exception("Attempted to load a QWORD into a register that only accepts a DWORD.");
                }
                if (p0 is float) {
                    m.SetRegister(p[0], (float)BitConverter.ToUInt64(m.Memory.GetRange((ulong)p1, 8), 0));
                    return;
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("LEARD", 0x4E, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = m.GetRegister(p[1]);
                if (p0 is ulong) {
                    m.SetRegister(p[0], (ulong)BitConverter.ToUInt32(m.Memory.GetRange((ulong)p1, 4), 0));
                    return;
                }
                if (p0 is uint) {
                    m.SetRegister(p[0], BitConverter.ToUInt32(m.Memory.GetRange((ulong)p1, 4), 0));
                }
                if (p0 is float) {
                    m.SetRegister(p[0], (float)BitConverter.ToUInt32(m.Memory.GetRange((ulong)p1, 4), 0));
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("LEARW", 0x4F, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = m.GetRegister(p[1]);
                if (p0 is ulong) {
                    m.SetRegister(p[0], (ulong)BitConverter.ToUInt16(m.Memory.GetRange((ulong)p1, 2), 0));
                    return;
                }
                if (p0 is uint) {
                    m.SetRegister(p[0], (uint)BitConverter.ToUInt16(m.Memory.GetRange((ulong)p1, 2), 0));
                }
                if (p0 is float) {
                    m.SetRegister(p[0], (float)BitConverter.ToUInt16(m.Memory.GetRange((ulong)p1, 2), 0));
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("LEARS", 0x50, 0x00000002, 2, new[] { typeof(byte), typeof(byte) }, (p,m) => {
                var p0 = m.GetRegister(p[0]);
                var p1 = m.GetRegister(p[1]);
                if (p0 is ulong) {
                    m.SetRegister(p[0], (ulong)BitConverter.ToSingle(m.Memory.GetRange((ulong)p1, 4), 0));
                    return;
                }
                if (p0 is uint) {
                    m.SetRegister(p[0], (uint)BitConverter.ToSingle(m.Memory.GetRange((ulong)p1, 4), 0));
                }
                if (p0 is float) {
                    m.SetRegister(p[0], BitConverter.ToSingle(m.Memory.GetRange((ulong)p1, 4), 0));
                }
            }, new OperandType[] { OperandType.Register, OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0]) + ", " + Machine.GetRegisterMnemonic(x[1])),

            new Instruction("PUSH", 0x51, 0x00000001, 1, new[] { typeof(byte) }, (p,m) => {
                m.PushRegister(p[0]);
            }, new OperandType[] { OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0])),

            new Instruction("POP", 0x52, 0x00000001, 1, new[] { typeof(byte) }, (p,m) => {
                m.PopRegister(p[0]);
            }, new OperandType[] { OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0])),
            new Instruction("JMPR", 0x53, 0x00000001, 1, new[] { typeof(byte) }, (p,m) => {
                m.SetRegister(0x30, (ulong)m.GetRegister(p[0]));
            }, new OperandType[] { OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0])),
            new Instruction("JNER", 0x54, 0x00000001, 1, new[] { typeof(byte) }, (p,m) => {
                if (m.CF != 1) m.SetRegister(0x30, (ulong)m.GetRegister(p[0]));
            }, new OperandType[] { OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0])),
            new Instruction("JER", 0x55, 0x00000001, 1, new[] { typeof(byte) }, (p,m) => {
                if (m.CF == 1) m.SetRegister(0x30, (ulong)m.GetRegister(p[0]));
            }, new OperandType[] { OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0])),
            new Instruction("JGR", 0x56, 0x00000001, 1, new[] { typeof(byte) }, (p,m) => {
                if (m.CF == 2) m.SetRegister(0x30, (ulong)m.GetRegister(p[0]));
            }, new OperandType[] { OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0])),
            new Instruction("JLR", 0x57, 0x00000001, 1, new[] { typeof(byte) }, (p,m) => {
                if (m.CF == 3) m.SetRegister(0x30, (ulong)m.GetRegister(p[0]));
            }, new OperandType[] { OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0])),
            new Instruction("JGER", 0x58, 0x00000001, 1, new[] { typeof(byte) }, (p,m) => {
                if (m.CF == 1 || m.CF == 2) m.SetRegister(0x30, (ulong)m.GetRegister(p[0]));
            }, new OperandType[] { OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0])),
            new Instruction("JLER", 0x59, 0x00000001, 1, new[] { typeof(byte) }, (p,m) => {
                if (m.CF == 1 || m.CF == 3) m.SetRegister(0x30, (ulong)m.GetRegister(p[0]));
            }, new OperandType[] { OperandType.Register }, x => Machine.GetRegisterMnemonic(x[0])),
            new Instruction("CALL", 0x5A, 0x00000008, 1, new[] { typeof(ulong) }, (p,m) => {
                m.PushRegister(0x30);
                m.SetRegister(0x30, BitConverter.ToUInt64(p,0));
            }, new OperandType[] { OperandType.QWORD }, x => "0x" + BitConverter.ToUInt64(x,0).ToString("X16")),
            new Instruction("RET", 0x5B, 0x00000000, 0, null, (p,m) => {
                m.PopRegister(0x30);
            }),
        };
    }
}



