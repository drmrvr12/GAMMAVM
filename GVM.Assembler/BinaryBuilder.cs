﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace GVM.Assembler {
    public class BinaryBuilder {
        private UInt64 pc = 0x0000000000000000;

        public BinaryBuilder(string[] file, IArchitecture architecture) {
            lines = file;
            Architecture = architecture;
        }
        public IArchitecture Architecture { get; }
        public Instruction[] Instructions => Architecture.Instructions;
        public string[] lines;
        Dictionary<string, ulong> labels = new Dictionary<string, ulong>();
        List<byte> result = new List<byte>();
        public byte[] Build() {
            for (int labelpass = 0; labelpass <= 1; labelpass++) {
                for (int I = 0; I < lines.Length; I++) {
                    var l = lines[I].Trim().Split(';')[0].Trim();
                    if (String.IsNullOrWhiteSpace(l)) continue;

                    if (l.EndsWith(":") && labelpass == 0) {
                        labels.Add(l.Substring(0, l.Length - 1), pc);
                        continue;
                    }

                    foreach (var X in labels) {
                        String[] array = l.Split(' ');
                        for (Int32 j = 0; j < array.Length; j++) {
                            var Y = array[j];
                            if (Y == X.Key) {
                                l = l.Replace(X.Key, X.Value + "");
                                break; // Don't break the foreach as a directive theoretically may contain two or more labels, depends on the architecture.
                            }
                        }
                    }
                    l = RegexASCII.Replace(l, match => {
                        StringBuilder sb = new StringBuilder();
                        byte[] bytes = System.Text.Encoding.ASCII.GetBytes(match.Groups[1].Value);
                        foreach (byte X in bytes) {
                            sb.Append(X.ToString() + ",");
                        }
                        return sb.ToString().TrimEnd(',', ' ');
                    });
                    if (l.ToUpper().StartsWith("DB")) {
                        string[] arr = l.Substring(2).Trim().Split(',');
                        for (int L = 0; L < arr.Length; L++) {
                            result.Add(Convert.ToByte(GetDecimalValue(arr[L].Trim())));
                            pc++;
                        }
                        continue;
                    }
                    if (l.ToUpper().StartsWith("DW")) {
                        string[] arr = l.Substring(2).Trim().Split(',');
                        for (int L = 0; L < arr.Length; L++) {
                            result.AddRange(BitConverter.GetBytes(Convert.ToUInt16(GetDecimalValue(arr[L].Trim()))));
                            pc += 2;
                        }
                        continue;
                    }
                    if (l.ToUpper().StartsWith("DW")) {
                        string[] arr = l.Substring(2).Trim().Split(',');
                        for (int L = 0; L < arr.Length; L++) {
                            result.AddRange(BitConverter.GetBytes(Convert.ToUInt32(GetDecimalValue(arr[L].Trim()))));
                            pc += 4;
                        }
                        continue;
                    }
                    if (l.ToUpper().StartsWith("DQ")) {
                        string[] arr = l.Substring(2).Trim().Split(',');
                        for (int L = 0; L < arr.Length; L++) {
                            result.AddRange(BitConverter.GetBytes(Convert.ToUInt64(GetDecimalValue(arr[L].Trim()))));
                            pc += 8;
                        }
                        continue;
                    }
                    if (l.ToUpper().StartsWith("DS")) {
                        string[] arr = l.Substring(2).Trim().Split(',');
                        for (int L = 0; L < arr.Length; L++) {
                            result.AddRange(BitConverter.GetBytes(Convert.ToSingle(GetDecimalValue(arr[L].Trim()))));
                            pc += 4;
                        }
                        continue;
                    }
                    for (int J = 0; J < Instructions.Length; J++) {
                        var it = Instructions[J];
                        int sbs = l.Length;
                        if (l.Contains(" ")) sbs = l.IndexOf(' ');
                        if (l.ToUpper().Substring(0, sbs).Trim() == it.Mnemonic) {
                            result.Add(it.Opcode);
                            pc++;
                            pc += it.Length;
                            if (it.OperandTypes == null || labelpass == 0) break;
                            var lp = l.Substring(it.Mnemonic.Length).Trim().Split(',');
                            for (int K = 0; K < lp.Length; K++) {
                                var arg = lp[K];
                                var argp = GetDecimalValue(arg);
                                if (argp == null) {
                                    RaiseError(l, arg, I, "\"" + arg.Trim() + "\" is not a valid number.");
                                    return null;
                                }
                                try {
                                    switch (it.OperandTypes[K]) {
                                        case OperandType.Register:
                                            result.Add(GVM.Machine.GetRegisterID(arg.Trim().ToUpper()));
                                            break;
                                        case OperandType.QWORD:
                                            result.AddRange(BitConverter.GetBytes(Convert.ToUInt64(argp)));
                                            break;
                                        case OperandType.DWORD:
                                            result.AddRange(BitConverter.GetBytes(Convert.ToUInt32(argp)));
                                            break;
                                        case OperandType.WORD:
                                            result.AddRange(BitConverter.GetBytes(Convert.ToUInt16(argp)));
                                            break;
                                        case OperandType.BYTE:
                                            result.Add(Convert.ToByte(argp));
                                            break;
                                        case OperandType.SINGLE:
                                            result.AddRange(BitConverter.GetBytes(Convert.ToSingle(arg)));
                                            break;
                                        default:
                                            break;
                                    }
                                } catch (Exception x) {
                                    RaiseError(l, arg, I, x.Message, x.ToString());
                                    return null;
                                }
                            }
                            break;
                        }
                    }
                } // ENDOF: for (lines.Length)
                if (labelpass == 0) {
                    pc = 0;
                    result.Clear();
                }
            } // ENDOF: for (labelpass)
            return result.ToArray();
        }
        private static readonly Regex RegexASCII = new Regex(@"\'([^%]+)\'");
        private static string GetDecimalValue(string input) {
            try {
                if (input.Trim().ToLower().StartsWith("0x")) {
                    return Convert.ToUInt64(input.Trim().Substring(2), 16) + "";
                }
                if (input.Trim().ToLower().StartsWith("0o")) {
                    return Convert.ToUInt64(input.Trim().Substring(2), 8) + "";
                }
                if (input.Trim().ToLower().StartsWith("0b")) {
                    return Convert.ToUInt64(input.Trim().Substring(2), 2) + "";
                }
            } catch (Exception x) {
                return null;
            }
            return input;
        }
        public event EventHandler<ErrorEventArgs> Error;
        protected void RaiseError(string lineText, string arg, int line, string message, string messageLate = null, string reserved = null) {
            Error?.Invoke(this, new ErrorEventArgs(lineText, arg, line, message));
        }
        public ulong PC {
            get {
                return pc;
            }
        }

    }
}
