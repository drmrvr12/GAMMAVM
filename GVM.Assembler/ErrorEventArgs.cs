﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GVM.Assembler {
    public class ErrorEventArgs : EventArgs {
        public ErrorEventArgs(String lineText, String argument, Int32 lineNumber, String message) {
            this.LineText = lineText;
            this.Argument = argument;
            this.LineNumber = lineNumber;
            this.Message = message;
        }

        public string LineText { get; }
        public string Argument { get; }
        public int LineNumber { get; }
        public string Message { get; }
    }
}
