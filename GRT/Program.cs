﻿/*
    ISC-No-Nuclear License

    Copyright (c) 2022 - 2023 Wiktor Cielebon

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.

    You acknowledge that this software is not designed, licensed or intended for 
    use in the design, construction, operation or maintenance of any nuclear facility.

 */

using GVM;
using System;
using System.Collections.Generic;
using System.Text;

namespace GRT {
    partial class Program {
        static Machine machine;
        static StringBuilder dasm;
        static void Main(string[] args) {
            if (args.Length == 0) {
                ShowHelp();
                return;
            } else {
                string file = "";
                bool hasargs = false;
                ulong nMaxMemory = 1024 * 1024 * 1024;
                if (args.Length == 1) {
                    if (args[0].StartsWith("/")) {
                        hasargs = true;
                    } else {
                        file = args[0];
                    }
                } else hasargs = true;
                List<IModule> modules = new List<IModule>();
                bool haltExit = false;
                bool doRaiseDisassembly = false;
                if (hasargs) {
                    foreach (var X in args) {
                        switch (X.ToUpper()) {
                            case "/D":
                                dasm = new StringBuilder();
                                doRaiseDisassembly = true;
                                break;

                            case "/?":
                                ShowHelp();
                                Environment.Exit(0x00000000);
                                return;

                            case string w when w.StartsWith("/I:"):
                                file = GetStringFromArgument(X);
                                break;
                            case string w when w.StartsWith("/XMX:"): {
                                    string sta = GetStringFromArgument(X);
                                    if (!ulong.TryParse(sta, out nMaxMemory)) {
                                        Console.WriteLine("\"{0}\" is not a number.", sta);
                                        return;
                                    }
                                    if (nMaxMemory < 1024) {
                                        Console.WriteLine("Invalid max memory specified: \r\n  " +
                                            "\"{0}\" is smaller than a single block (1024 B)", sta);
                                        return;
                                    }
                                    break;
                                }
                            case string w when w.StartsWith("/MOD:"): {
                                    string sta = GetStringFromArgument(X);
                                    string comb = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), sta);
                                    string fn = "";
                                    fn = System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), sta);
                                    if (!System.IO.File.Exists(fn)) {
                                        if (System.IO.File.Exists(comb)) {
                                            fn = comb;
                                        } else {
                                            Console.WriteLine("File not found: \"" + sta + "\"");
                                            return;
                                        }
                                    }
                                    IModule mod = GetModuleFromFile(fn);
                                    if (mod == null) {
                                        Console.WriteLine("The module \"" + sta + "\" does not appear to have any public members implementing the IModule interface.");
                                    } else {
                                        modules.Add(mod);
                                    }
                                    break;
                                }
                            default:
                                Console.WriteLine("Invalid argument: \"{0}\".", X);
                                return;
                        }
                    }
                }
                if (string.IsNullOrWhiteSpace(file)) {
                    Console.WriteLine("Missing argument: \"/I:FILE\"");
                    return;
                }
                if (!System.IO.File.Exists(file)) {
                    Console.WriteLine("File not found: \"" + file + "\"");
                    return;
                }

                foreach (var X in modules) {
                    if (X.HaltsExit) {
                        haltExit = true;
                        break;
                    }
                }

                machine = new Machine(new GAMMA64.StdGAMMA64(), nMaxMemory);
                machine.Interrupt += InterruptHandler;
                machine.FatalError += Machine_FatalError;
                machine.OpcodeNotFound += Machine_OpcodeNotFound;

                foreach (var X in modules) {
                    X.Caller = machine;
                    X.Load();
                }

                if (doRaiseDisassembly) {
                    machine.RaiseDisassembly = true;
                    machine.Disassembly += Machine_Disassembly;
                }
                machine.Execute(System.IO.File.ReadAllBytes(file));
                if (haltExit) {
                    Console.ResetColor();
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.Write("One or more modules have requested to keep this program running." +
                        "\r\nReview the module output and then press any key to exit...");
                    Console.ResetColor();
                    Console.ReadKey(true);
                    Console.WriteLine();
                }

                foreach (var X in modules)
                    X.End();
            }
        }
        

        static void ShowHelp() {
            Console.WriteLine("Usage:" +
                    "\r\n" +
                    "\r\n   GRT [/?] [/D] [/XMX:MEMORY] [/MOD:FILENAME] /I:FILE" +
                    "\r\n" + 
                    "\r\n   GRT [FILE]" +
                    "\r\n" +
                    "\r\n/? - Show this text" +
                    "\r\n/D - Show disassembly" +
                    "\r\n/XMX:MEMORY - Max memory size (in bytes)" +
                    "\r\n/MOD:FILENAME - Load a debugger module" +
                    "\r\n/I:FILE -  Compiled bytecode (typically *.BIN)." +
                    "\r\n\r\nor\r\n" +
                    "\r\nFILE -  Compiled bytecode (typically *.BIN). Must be the only argument");
        }

        static string GetStringFromArgument(string w) => w.Substring(w.IndexOf(':') + 1).Trim();

        #region Handlers
    
        private static void Machine_OpcodeNotFound(Object sender, OpcodeNotFoundEventArgs e) {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Invalid opcode: 0x" + e.Opcode.ToString("X2") + " at offset 0x" + ((Machine)sender).PC.Peek().ToString("X16"));
            Console.ResetColor();
        }

        private static void Machine_FatalError(Object sender, FatalErrorEventArgs e) {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Fatal error at offset 0x" + machine.PC.Peek().ToString("X16") + ":\r\n  " + e.Exception);
            Console.ResetColor();
            Redmp:
            Console.WriteLine("D - Dump memory, A - Abort");
            var k = Console.ReadKey();
            Console.WriteLine();
            switch (k.Key) {
                case ConsoleKey.A:
                    return;
                case ConsoleKey.D:
                    byte[] bytes = new byte[machine.Memory.BlocksAllocated * machine.Memory.BlockSize];
                    for (ulong M = 0; M < machine.Memory.BlocksAllocated * machine.Memory.BlockSize; M++) {
                        bytes[M] = machine.Memory[M];
                    }
                    string dt = DateTime.Now.ToString("r").Replace(":", "_");
                    Console.WriteLine("Saving...");
                    System.IO.File.WriteAllBytes(dt + ".DMP", bytes);
                    if (dasm != null)
                        System.IO.File.WriteAllText(dt + ".ASM", dasm.ToString());
                    Console.WriteLine("Saved dump as \"" + dt + ".DMP\".");
                    if (dasm != null)
                        Console.WriteLine("Saved disassembly as \"" + dt + ".ASM\".");
                    break;
                default:
                    goto Redmp;
            }
        }

        private static void Machine_Disassembly(Object sender, DisassemblyEventArgs e) {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("0x" + e.PC.ToString("X16"));
            Console.ResetColor();

            StringBuilder sb = new StringBuilder();

            sb.Append(e.Opcode.ToString("X2") + " ");
            for (int K = 0; K < e.Args.Length; K++) {
                sb.Append(e.Args[K].ToString("X2") + " ");
            }
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" [" + sb.ToString().TrimEnd() + "] ");
            Console.ResetColor();

            Console.WriteLine(e.Disassembly);

            dasm.AppendLine(e.Disassembly + " ; 0x" + e.PC.ToString("X16") + " [" + sb.ToString().TrimEnd() + "]");
        }

        #endregion

        static IModule GetModuleFromFile(string Filename) {
            if (System.IO.File.Exists(Filename)) {
                Type T = typeof(IModule);
                System.Reflection.Assembly a = null;
                try {
                    a = System.Reflection.Assembly.LoadFrom(Filename);
                } catch (BadImageFormatException) {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("The module \"" + Filename + "\" is not a valid CLR assembly.");
                    Console.ResetColor();
                    return null;
                }
                foreach (Type TP in a.GetTypes()) {
                    if (TP.IsNotPublic)
                        continue;
                    if (T.IsAssignableFrom(TP)) {
                        IModule arch = (IModule)Activator.CreateInstance(TP);
                        return arch;
                    }
                }
            }
            return null;
        }
    }

    public interface IModule {
        void Load();
        void End();
        Machine Caller { get; set; }
        bool HaltsExit { get; }
    }
}
