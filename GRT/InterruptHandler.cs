﻿/*
    ISC-No-Nuclear License

    Copyright (c) 2022 - 2023 Wiktor Cielebon

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH
    REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY
    AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
    INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM
    LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR
    OTHER TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
    PERFORMANCE OF THIS SOFTWARE.

    You acknowledge that this software is not designed, licensed or intended for 
    use in the design, construction, operation or maintenance of any nuclear facility.

 */

using GVM;
using System;

namespace GRT {
    partial class Program {
        public static void InterruptHandler(object sender, InterruptEventArgs e) {
            Machine m = (Machine)sender;
            switch (e.Code) {
                case 0x10: { // Console routines
                        switch (m.R0.Peek()) {
                            //
                            // INT 0x10 Function 0x00000001 - Print an ASCII character stored in the R1 register to the 
                            // console output stream at the current cursor position.
                            //
                            // IN: R1 - Character
                            //
                            case 0x00000001:
                                Console.Write(System.Text.Encoding.ASCII.GetChars(new[] { Convert.ToByte(m.R1.Peek()) }));
                                break;
                            //
                            // INT 0x10 Function 0x00000002 - Change the console color
                            //
                            // IN: R1 - Color (Same as BIOS color)
                            //
                            case 0x00000002:
                                byte color = Convert.ToByte(m.R1.Peek());
                                Console.BackgroundColor = (System.ConsoleColor)(color >> 4);
                                Console.ForegroundColor = (System.ConsoleColor)((byte)(color << 4) >> 4);
                                break;
                            //
                            // INT 0x10 Function 0x00000003 - Clear the console buffer
                            //
                            case 0x00000003:
                                Console.Clear();
                                break;
                            //
                            // INT 0x10 Function 0x00000004 - Set cursor position
                            //
                            // IN: R1 - Left; R2 - Top
                            //
                            case 0x00000004:
                                Console.SetCursorPosition((int)m.R1.Peek(), (int)m.R2.Peek());
                                break;
                            //
                            // INT 0x10 Function 0x00000005 - Set buffer size
                            //
                            // IN: R1 - Cols; R2 - Rows
                            //
                            case 0x00000005:
                                Console.SetBufferSize((int)m.R1.Peek(), (int)m.R2.Peek());
                                break;
                            //
                            // INT 0x10 Function 0x00000006 - Set window size
                            //
                            // IN: R1 - Cols; R2 - Rows
                            //
                            case 0x00000006:
                                Console.SetWindowSize((int)m.R1.Peek(), (int)m.R2.Peek());
                                break;
                            //
                            // INT 0x10 Function 0x00000007 - Get window size
                            //
                            // OUT: R1 - Cols; R2 - Rows
                            //
                            case 0x00000007:
                                m.SetRegister(0x01, (uint)Console.WindowWidth);
                                m.SetRegister(0x02, (uint)Console.WindowHeight);
                                break;
                            //
                            // INT 0x10 Function 0x00000008 - Get buffer size
                            //
                            // OUT: R1 - Cols; R2 - Rows
                            //
                            case 0x00000008:
                                m.SetRegister(0x01, (uint)Console.BufferWidth);
                                m.SetRegister(0x02, (uint)Console.BufferHeight);
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                case 0x11: { // Device input routines
                        switch (m.R0.Peek()) {
                            //
                            // INT 0x11 Function 0x00000001 - Read keyboard key and store the scancode in R2
                            //
                            case 0x00000001:
                                var ki = Console.ReadKey(true);
                                if (ki.Key == ConsoleKey.Enter) {
                                    m.SetRegister(0x03, (uint)0x0D);
                                } else {
                                    m.SetRegister(0x03, (uint)ki.KeyChar);
                                }
                                m.SetRegister(0x02, (uint)ki.Key);
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                case 0x12: { // QOL Stuff
                        switch (m.R0.Peek()) {
                            //
                            // INT 0x11 Function 0x00000001 - Write the number from a register to memory as NUL-terminated ASCII string
                            //
                            case 0x00000001:
                                byte[] num = System.Text.Encoding.ASCII.GetBytes(m.RX0.Peek().ToString());
                                ulong i = 0;
                                for (; i < (ulong)num.LongLength; i++)
                                    m.Memory[m.RX1.Peek() + i] = num[i];
                                m.Memory[m.RX1.Peek() + i++] = 0x00;
                                break;
                            default:
                                break;
                        }
                        break;
                    }
                default:
                    break;
            }
        }
    }
}
